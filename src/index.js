import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import CountingMachine from "./components/task1.1ai";
import PhotoPage from "./components/task1.1b.photoPage";
import VisitorSystem from "./components/task1.1c.VisitorSystem";
import PhotoPage2 from "./components/task1.2.photoPage";
import VisitorSystem3 from "./components/task1.3.VisitorSystem";
import PlayerSystem from "./components/task2.1.PlayerSystem";
import PlayerSystem2 from "./components/task2.2.PlayerSystem2";
import PlayerSystem3 from "./components/task2.3.PlayerSystem3";
import PlayerSystem4 from "./components/task2.4.PlayerSystem4";
import MainComponent from "./components/task3.MainComponent";
import LibrarySystem from "./components/task4.1.LibrarySystem";
import LibrarySystem2 from "./components/task4.2.LibrarySystem2";
import LibrarySystem3 from "./components/task4.3.LibrarySystem3";
import FirstComponent from "./components/task5.FirstComponent";
import Products from "./components/task6.1.Products";
import Products2 from "./components/task6.2.Products2";
import Products3 from "./components/task6.3.Products3";
import MessagingFolder from "./components/task7.1.MessagingFolder";
import MessagingFolder2 from "./components/task7.2.MessagingFolder2";

ReactDOM.render(
  <React.StrictMode>
    {/* <App /> */}
    {/* <CountingMachine/> */}
    {/* <PhotoPage /> */}
    {/* <VisitorSystem /> */}
    {/* <PhotoPage2 /> */}
    {/* <VisitorSystem3 /> */}
    {/* <PlayerSystem /> */}
    {/* <PlayerSystem2 /> */}
    {/* <PlayerSystem3 /> */}
    {/* <PlayerSystem4 /> */}
    {/* <MainComponent /> */}
    {/* <LibrarySystem /> */}
    {/* <LibrarySystem2 /> */}
    {/* <LibrarySystem3 /> */}
    {/* <FirstComponent/> */}
    {/* <Products /> */}
    {/*  <Products2 /> */}
    {/* <Products3 /> */}
    {/* <MessagingFolder /> */}
    <MessagingFolder2 />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
