import React, { Component } from "react";
import Store from "./task6.1.Store";
class Products extends Component {
  state = {
    ProductsArr: [
      { code: "c332", name: "Coca Cola", price: 20, qty: 10 },
      { code: "F168", name: "5Star", price: 15, qty: 0 },
      { code: "M228", name: "Maggi", price: 28, qty: 22 },
      { code: "P288", name: "Pepsi", price: 20, qty: 18 },
      { code: "D311", name: "Dairy Milk", price: 40, qty: 0 },
      { code: "M301", name: "Mirinda", price: 25, qty: 8 },
      { code: "K477", name: "Kitkat", price: 16, qty: 11 },
      { code: "R544", name: "Red Bull", price: 90, qty: 3 },
    ],
  };
  handelIncreas = (Code) => {
    let s1 = { ...this.state };
    let prod = s1.ProductsArr.find((p1) => p1.code === Code);
    prod.qty++;
    this.setState(s1);
  };

  handelDecreas = (Code) => {
    let s1 = { ...this.state };
    let prod = s1.ProductsArr.find((p1) => p1.code === Code);
    prod.qty--;
    this.setState(s1);
  };
  render() {
    let { ProductsArr } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <h2 className="text-center">Products in store </h2>
          <div className="row">
            {ProductsArr.map((p1) => {
              return (
                <Store
                  Product={p1}
                  onIncrease={this.handelIncreas}
                  onDecrease={this.handelDecreas}
                />
              );
            })}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Products;
