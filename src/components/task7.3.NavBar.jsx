import React, { Component } from "react";
class EmailNavbar extends Component {
  render() {
    let { noOfMessage, noOfUnreadMessage} = this.props;

    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark px-5">
          <a className="navbar-brand" href="#">
            Email
          </a>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Messages
                  <span className="badge badge-pill badge-secondary">
                    {noOfMessage}
                  </span>
                </a>
              </li>

              <li className="nav-item">
                <a className="nav-link" href="#">
                  Unread Message
                  <span className="badge badge-pill badge-secondary">
                    {noOfUnreadMessage}
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}
export default EmailNavbar;
