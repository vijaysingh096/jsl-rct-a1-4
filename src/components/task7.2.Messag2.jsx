import React, { Component } from "react";
class Message2 extends Component {
  render() {
    let { message ,onBack,onDelete } = this.props;
    let { id, from, to, subject, text } = message;
    let txtArr = text.split("\n");
    // console.log(txtArr);
    return (
      <React.Fragment>
        <div className="row border px-3">
          <div className="col-6 text-start">
            <i class="fas fa-long-arrow-alt-left" onClick={()=>onBack()}></i>
          </div>
          <div className="col-6 text-end">
            <i class="fas fa-trash-alt" onClick={()=>onDelete(id)}></i>
          </div>
        </div>
        <div className="row border px-3">
          <div className="col">
            From : {from}
            <br />
            To : {to}
            <br />
            Subject : {subject}
            <br />
            {txtArr.map((t1) => (
              <>
                {t1} <br />
              </>
            ))}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Message2;
