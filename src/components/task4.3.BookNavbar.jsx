import React, { Component } from "react";
class BookNavbar extends Component {
  render() {
    let { noOfBooks, noOfIssueBook } = this.props;

    return (
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
          <a className="navbar-brand" href="#">
            Library
          </a>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Books
                  <span className="badge badge-pill badge-secondary">
                    {noOfBooks}
                  </span>
                </a>
              </li>

              <li className="nav-item">
                <a className="nav-link" href="#">
                  Issued Books
                  <span className="badge badge-pill badge-secondary">
                    {noOfIssueBook}
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}
export default BookNavbar;
