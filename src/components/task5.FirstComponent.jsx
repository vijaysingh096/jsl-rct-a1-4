import React, { Component } from "react";
import SecondComponent from "./task5.SecondComponent";
class FirstComponent extends Component {
  state = {
    Mobiles: [
      {
        name: "Redmi",
        RAM: "Upto 64GB",
        features: "Dual Rear Cam|3000mAh",
        price: 7499,
      },
      {
        name: "Realme",
        RAM: "Upto 4GB",
        features: "Dual Rear Cam|4230mAh",
        price: 8999,
      },
      {
        name: "Honor 7S",
        RAM: "2GB|16GB",
        features: "Face Unlock|3020mAh",
        price: 5499,
      },
      {
        name: "Samsung J6",
        RAM: "4GB|64GB",
        features: "14.22cm HD | Face Unlock",
        price: 9490,
      },
      {
        name: "Moto One",
        RAM: "4GB|64GB",
        features: "Extra 2000 off on Exchange",
        price: 13999,
      },
      {
        name: "Nokia 6.1",
        RAM: "3GB|32GB",
        features: "Full HD Display|SD",
        price: 6999,
      },
    ],
    cart: [],
  };
  handelAddCart = (Name) => {
    let s1 = { ...this.state };
    let m1 = s1.cart.find((m) => m.name === Name);
    if (m1) {
      m1.qty++;
      m1.value = m1.value + m1.price;
    } else {
      let mob = s1.Mobiles.find((m) => m.name === Name);
      let newArr = {
        name: mob.name,
        RAM: mob.RAM,
        features: mob.features,
        price: mob.price,
        qty: 0,
        value: 0,
      };
      newArr.qty = 1;
      newArr.value = newArr.qty * newArr.price;
      s1.cart.push(newArr);
    }
    this.setState(s1);
  };

  removeFromCart = (Name) => {
    let s1 = { ...this.state };
    let index = s1.cart.findIndex((m) => m.name === Name);
    s1.cart.splice(index, 1);
    this.setState(s1);
  };

  render() {
    let { Mobiles, cart } = this.state;
    let cartValue = cart.reduce((acc, curr) => acc + curr.value, 0);
    return (
      <React.Fragment>
        <div className="container">
          <h4 className="text-center">Exciting Deals on Mobiles</h4>
          <div className="row ">
            {Mobiles.map((m1) => {
              return (
                <SecondComponent mobile={m1} addCart={this.handelAddCart} />
              );
            })}
          </div>
          <h2>Cart</h2>
          {cart.length <= 0 ? (
            <p>Cart is Empty</p>
          ) : (
            <>
              <ul>
                {cart.map((m1) => {
                  return (
                    <li>
                      {m1.name}, quantity: {m1.qty}{" "}
                      <button
                        className="btn btn-secondary m-2 btn-sm"
                        onClick={() => this.removeFromCart(m1.name)}
                      >
                        Remove from Cart
                      </button>{" "}
                    </li>
                  );
                })}
              </ul>
              Number of items in cart = {cart.length}
              <br />
              Value of cart = {cartValue}
            </>
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default FirstComponent;
