import React, { Component } from "react";
class NavBar extends Component {
  render() {
    let { Count, qty } = this.props;

    return ( 
      <React.Fragment>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
          <a className="navbar-brand" href="#">
            MySystem
          </a>
          <div className="" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Products
                  <span className="badge badge-pill badge-secondary">
                    {Count}
                  </span>
                </a>
              </li>

              <li className="nav-item">
                <a className="nav-link" href="#">
                  Quantity
                  <span className="badge badge-pill badge-secondary">
                    {qty}
                  </span>
                </a>
              </li>

            </ul>
          </div>
        </nav>
      </React.Fragment>
    );
  }
}
export default NavBar;
