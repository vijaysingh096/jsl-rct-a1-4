import React, { Component } from "react";
class Counter extends Component {
  state = {  };

 

  render() {
    console.log("Props", this.props);
    const { counter, onDelete,onIncrement } = this.props;
    const {alphabet,count,id}=counter
    return (
      <React.Fragment>
        <h4>
          Counter {alphabet} Count={count}
        </h4>
        <button
          className="btn btn-primary btn-sm m-1"
          onClick={() => onIncrement(id)}
        >
          Increment
        </button>
        <button
          className="btn btn-danger btn-sm m-1"
          onClick={() => onDelete(id)}
        >
          Delete
        </button>
      </React.Fragment>
    );
  }
}
export default Counter;
