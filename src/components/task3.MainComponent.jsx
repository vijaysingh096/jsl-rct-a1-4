import React, { Component } from "react";
import NavBar from "./task3.NabBar";
class MainComponent extends Component {
  render() {
    let count = 10;
    let qty = 24;
    return (
      <React.Fragment>
        <NavBar Count={count} qty={qty} /> 
        <div className="container">
          <h6>Count = {count}</h6>
          <h6>Quantity = {qty} </h6>
        </div>
      </React.Fragment>
    );
  }
}
export default MainComponent;
