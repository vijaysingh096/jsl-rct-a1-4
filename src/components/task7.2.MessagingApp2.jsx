import React, { Component } from "react";
class MessagingApp2 extends Component {
  render() {
    let { email,onRead } = this.props;
    let { id, from, subject, read } = email;
    return (
      <React.Fragment>
        <div className="row border p-2 " onClick={()=>onRead(id)}>
          <div className="col-2">
            <i
              class={
                read === false ? "fas fa-envelope" : "far fa-envelope-open"
              }
            ></i>
          </div>
          <div className="col-5"> {read === false ? <b>{from}</b> : from}</div>
          <div className="col-5">
            {" "}
            {read === false ? <b>{subject}</b> : subject}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default MessagingApp2;
