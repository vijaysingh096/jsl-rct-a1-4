import React, { Component } from "react";

class PhotoComp extends Component {
  bgColor = (para) => {
    let clname =
      para === "like"
        ? "bg-success"
        : para === "dislike"
        ? "bg-warning"
        : "bg-light";
    return clname;
  };
  render() {
    const { photo, index, onLike, onDislike } = this.props;
    const { img, like, dislike, myOption } = photo;
    return (
      <React.Fragment>
        <div className={"col-4  border text-center " + this.bgColor(myOption)}>
          {/* {console.log("col-4 " + this.bgColor(myOption), myOption)} */}
          <img src={img}></img>
          <br />
          <i
            className={
              myOption === "like"
                ? "fas fa-thumbs-up m-1"
                : "far fa-thumbs-up m-1"
            }
            onClick={() => onLike(index)}
          >
            {" "}
            {like}{" "}
          </i>

          <i
            className={
              myOption === "dislike"
                ? "fas fa-thumbs-down m-1"
                : "far fa-thumbs-down m-1"
            }
            onClick={() => onDislike(index)}
          >
            {" "}
            {dislike}{" "}
          </i>
        </div>
      </React.Fragment>
    );
  }
}
export default PhotoComp;
