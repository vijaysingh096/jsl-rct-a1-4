import React, { Component } from "react";

class PhotoComp2 extends Component {
  bgColor = (para) => {
    let clname =
      para === "like"
        ? "bg-success"
        : para === "dislike"
        ? "bg-warning"
        : "bg-light";
    return clname;
  };
  render() {
    const { photo, index, onLike, onDislike, onDelete } = this.props;
    const { img, like, dislike, myOption } = photo;
    return (
      <React.Fragment>
        <div className={"col-4  border text-center " + this.bgColor(myOption)}>
          {/* {console.log("col-4 " + this.bgColor(myOption), myOption)} */}
          <img src={img}></img>
          <br />
          <i
            className={
              myOption === "like"
                ? "fas fa-thumbs-up m-1"
                : "far fa-thumbs-up m-1"
            }
            onClick={() => onLike(index)}
          >
            {" "}
            {like}{" "}
          </i>

          <i
            className={
              myOption === "dislike"
                ? "fas fa-thumbs-down m-1"
                : "far fa-thumbs-down m-1"
            }
            onClick={() => onDislike(index)}
          >
            {" "}
            {dislike}{" "}
          </i>

          <i class="far fa-trash-alt" onClick={() => onDelete(index)}></i>
        </div>
      </React.Fragment>
    );
  }
}
export default PhotoComp2;
