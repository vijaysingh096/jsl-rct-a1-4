import React, { Component } from "react";
import Counter from "./counter";
class CountingMachine extends Component {
  state = {
    counters: [
      { id: 12, alphabet: "A", count: 0 },
      { id: 23, alphabet: "B", count: 0 },
      { id: 7, alphabet: "C", count: 0 },
      { id: 16, alphabet: "D", count: 0 },
    ],
    data:""
  };

handelIncrement=(id)=>{
    let s1 = { ...this.state };
    let counter = s1.counters.find((c1) => c1.id === id);
    counter.count++;
    s1.data+=counter.alphabet
    this.setState(s1);
}

  handelDelete = (id) => {
    let s1 = { ...this.state };
    let index = s1.counters.findIndex((c1) => c1.id === id);
    s1.counters.splice(index, 1);
    this.setState(s1);
  };

  handelReset = () => {
    let s1 = { ...this.state };
    const arr1 = s1.counters.map((c1) => ({
      id: c1.id,
      alphabet: c1.alphabet,
      count: 0,
    }));
    s1.counters = arr1;
    s1.data=""
    this.setState(s1);
  };

  render() {
    const { counters,data } = this.state;
    return (
      <React.Fragment>
        {counters.map((c1) => {
          return <Counter counter={c1} onDelete={this.handelDelete} onIncrement={this.handelIncrement} />;
        })}
        <br />
        <button
          className="btn btn-primary btn-sm m-1"
          onClick={() => this.handelReset()}
        >
          Reset All
        </button>

        <h5>Alphabet : {data}</h5>
      </React.Fragment>
    );
  }
}
export default CountingMachine;
