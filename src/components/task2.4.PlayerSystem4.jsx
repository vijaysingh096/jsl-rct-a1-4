import React, { Component } from "react";
import Player4 from "./task2.4.Player4";
class PlayerSystem4 extends Component {
  state = {
    Players: [
      { name: "Jack", score: 21 },
      { name: "Steve", score: 30 },
      { name: "Martha", score: 44 },
      { name: "Bob", score: 15 },
      { name: "Katherine", score: 28 },
    ],
  };
  handelPoint = (Name) => {
    let s1 = { ...this.state };
    let player1 = s1.Players.find((p1) => p1.name === Name);
    // console.log(player1);
    player1.score++;
    this.setState(s1);
  };
  render() {
    let { Players } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <h3>List of Name</h3>
          {Players.map((p1) => {
            return <Player4 player={p1} addPoint={this.handelPoint} />;
          })}
        </div>
      </React.Fragment>
    );
  }
}
export default PlayerSystem4;
