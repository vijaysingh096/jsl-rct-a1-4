import React, { Component } from "react";
class Player4 extends Component {
  render() {
    let { player,addPoint } = this.props;
    let { name, score } = player;
    return (
      <React.Fragment>
        <div className="col  text-primary">
          Name : {name}<br/> Score:{score}<br/>
          <button className="btn btn-success btn-sm " onClick={() => addPoint(name)} >1 Point</button>
        </div>
      </React.Fragment>
    );
  }
}
export default Player4;
