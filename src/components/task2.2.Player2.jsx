import React, { Component } from "react";
class Player2 extends Component {
  render() {
    let { player } = this.props;
    return (
      <React.Fragment>
        <p className="bg-dark text-white">{player}</p>
      </React.Fragment>
    );
  }
}
export default Player2;
