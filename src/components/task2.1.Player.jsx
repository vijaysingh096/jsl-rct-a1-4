import React, { Component } from "react";
class Player extends Component {
  render() {
    let { player } = this.props;
    return (
      <React.Fragment>
        <p className="text-primary">{player}</p>
      </React.Fragment>
    );
  }
}
export default Player;
