import React, { Component } from "react";
class Player3 extends Component {
  render() {
    let { player } = this.props;
    let { name, score } = player;
    return (
      <React.Fragment>
        <div className="col  text-primary">
          Name : {name}<br/> Score:{score}
        </div>
      </React.Fragment>
    );
  }
}
export default Player3;
