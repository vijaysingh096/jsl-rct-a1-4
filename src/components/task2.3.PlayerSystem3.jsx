import React, { Component } from "react";
import Player3 from "./task2.3.Player3";
class PlayerSystem3 extends Component {
  state = {
    Players: [
      { name: "Jack", score: 21 },
      { name: "Steve", score: 30 },
      { name: "Martha", score: 44 },
      { name: "Bob", score: 15 },
      { name: "Katherine", score: 28 },
    ],
  };
  render() {
    let { Players } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <h3>List of Name</h3>
          {Players.map((p1) => {
            return <Player3 player={p1} />;
          })}
        </div>
      </React.Fragment>
    );
  }
}
export default PlayerSystem3;
