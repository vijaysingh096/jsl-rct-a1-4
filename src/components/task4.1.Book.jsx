import React, { Component } from "react";
class Book extends Component {
  render() {
    let { book, onIssueBook } = this.props;
    let { name, author } = book;
    return (
      <React.Fragment>
        <div className="col-5 border my-1 mx-3 alert-danger">
          <h5>{name} </h5>
          {author}
          <br />
          <button
            className="btn bg-white m-2 btn-sm "
            onClick={() => onIssueBook(name)}
          >
            Issue
          </button>
        </div>
      </React.Fragment>
    );
  }
}
export default Book;
