import React, { Component } from "react";
import Store3 from "./task6.3.Store3";
class Products3 extends Component {
  state = {
    ProductsArr: [
      { code: "c332", name: "Coca Cola", price: 20, qty: 10 },
      { code: "F168", name: "5Star", price: 15, qty: 0 },
      { code: "M228", name: "Maggi", price: 28, qty: 22 },
      { code: "P288", name: "Pepsi", price: 20, qty: 18 },
      { code: "D311", name: "Dairy Milk", price: 40, qty: 0 },
      { code: "M301", name: "Mirinda", price: 25, qty: 8 },
      { code: "K477", name: "Kitkat", price: 16, qty: 11 },
      { code: "R544", name: "Red Bull", price: 90, qty: 3 },
    ],
    viewIndex: -1,
  };
  handelIncreas = (Code) => {
    let s1 = { ...this.state };
    let prod = s1.ProductsArr.find((p1) => p1.code === Code);
    prod.qty++;
    this.setState(s1);
  };

  handelDecreas = (Code) => {
    let s1 = { ...this.state };
    let prod = s1.ProductsArr.find((p1) => p1.code === Code);
    prod.qty--;
    this.setState(s1);
  };

  orderByQnantity = () => {
    let s1 = { ...this.state };
    s1.ProductsArr = s1.ProductsArr.sort((p1, p2) => p1.qty - p2.qty);
    this.setState(s1);
  };

  orderByPrice = () => {
    let s1 = { ...this.state };
    s1.ProductsArr = s1.ProductsArr.sort((p1, p2) => p1.price - p2.price);
    this.setState(s1);
  };
  handelView = (index) => {
    let s1 = { ...this.State };
    s1.viewIndex = index < 0 ? 1 : -1;
    console.log(s1.viewIndex);
    this.setState(s1);
  };
  render() {
    let { ProductsArr, viewIndex } = this.state;
    let mapArr = ProductsArr.map((p1) => {
      return (   
        <Store3
          Product={p1}
          onIncrease={this.handelIncreas}
          onDecrease={this.handelDecreas}
          changeView={viewIndex}
        />
      ); 
    });
    return (
      <React.Fragment>
        <div className="container">
          <h2 className="text-center">Products in store </h2>
          <div className="text-center">
            <button
              className="btn btn-primary  m-1 "
              onClick={() => this.orderByQnantity()}
            >
              Order by Quantity
            </button>
            <button
              className="btn btn-primary  m-1 "
              onClick={() => this.orderByPrice()}
            >
              Order by Price
            </button>
            <button
              className="btn btn-primary  m-1 "
              onClick={() => this.handelView(viewIndex)}
            >
              {viewIndex > 0 ? "View:Grid" : "View:Table"}
            </button>
          </div>
          {viewIndex < 0 ? (
            <div className="row">{mapArr}</div>
          ) : (
            <>
              {" "}
              <div className="row border bg-dark text-white">
                <div className="col-3 ">Name</div>
                <div className="col-2 ">Code</div>
                <div className="col-2 ">Price</div>
                <div className="col-2 ">Quantity</div>
                <div className="col-3 "></div>
              </div>
              {mapArr}
            </>
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default Products3;
