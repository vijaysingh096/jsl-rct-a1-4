import React, { Component } from "react";
import Store2 from "./task6.2.Store2";
class Products2 extends Component {
  state = {
    ProductsArr: [
      { code: "c332", name: "Coca Cola", price: 20, qty: 10 },
      { code: "F168", name: "5Star", price: 15, qty: 0 },
      { code: "M228", name: "Maggi", price: 28, qty: 22 },
      { code: "P288", name: "Pepsi", price: 20, qty: 18 },
      { code: "D311", name: "Dairy Milk", price: 40, qty: 0 },
      { code: "M301", name: "Mirinda", price: 25, qty: 8 },
      { code: "K477", name: "Kitkat", price: 16, qty: 11 },
      { code: "R544", name: "Red Bull", price: 90, qty: 3 },
    ],
  };
  handelIncreas = (Code) => {
    let s1 = { ...this.state };
    let prod = s1.ProductsArr.find((p1) => p1.code === Code);
    prod.qty++;
    this.setState(s1);
  };

  handelDecreas = (Code) => {
    let s1 = { ...this.state };
    let prod = s1.ProductsArr.find((p1) => p1.code === Code);
    prod.qty--;
    this.setState(s1);
  };

  orderByQnantity = () => {
    let s1 = { ...this.state };
    s1.ProductsArr = s1.ProductsArr.sort((p1, p2) => p1.qty - p2.qty);
    this.setState(s1);
  };

  orderByPrice = () => {
    let s1 = { ...this.state };
    s1.ProductsArr = s1.ProductsArr.sort((p1, p2) => p1.price - p2.price);
    this.setState(s1);
  };
  render() {
    let { ProductsArr } = this.state;
    return (
      <React.Fragment>
        <div className="container">
          <h2 className="text-center">Products in store </h2>
          <div className="text-center">
            <button
              className="btn btn-primary  m-1 "
              onClick={() => this.orderByQnantity()}
            >
              Order by Quantity
            </button>
            <button
              className="btn btn-primary  m-1 "
              onClick={() => this.orderByPrice()}
            >
              Order by Price
            </button>
          </div>
          <div className="row">
            {ProductsArr.map((p1) => {
              return (
                <Store2
                  Product={p1}
                  onIncrease={this.handelIncreas}
                  onDecrease={this.handelDecreas}
                />
              );
            })}
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default Products2;
