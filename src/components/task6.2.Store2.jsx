import React, { Component } from "react";
class Store2 extends Component {
  render() {
      let {Product ,onIncrease ,onDecrease}=this.props
      let {code,name,price,qty}=Product
    return <React.Fragment>
         <div className="col-3 my-2">
          <div class="card bg-success text-center">
            <div class="card-body">
              <h5 class="card-title">
                {name}
              </h5>
              <p class="card-text">Code: {code}</p>
              <p class="card-text">Price: {price}</p>
              <p class="card-text">Quantity: {qty}</p>
              <button
                className="btn bg-white m-1 btn-sm"
                onClick={() => onIncrease(code)}
              >
                Increase
              </button>
              <button
                className={qty>0?"btn bg-white m-1 btn-sm":"btn bg-white m-1 disabled btn-sm"}
                onClick={() => onDecrease(code)}
              >
                Decrease
              </button>
            </div>
          </div>
        </div>
    </React.Fragment>;
  }
}
export default Store2;
