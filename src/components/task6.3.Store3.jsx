import React, { Component } from "react";
class Store3 extends Component {
  render() {
    let { Product, onIncrease, onDecrease, changeView } = this.props;
    let { code, name, price, qty } = Product;
    return (
      <React.Fragment>
        {changeView < 0 ? (
          <div className="col-3 my-2">
            <div class="card bg-success text-center">
              <div class="card-body">
                <h5 class="card-title">{name}</h5>
                <p class="card-text">Code: {code}</p>
                <p class="card-text">Price: {price}</p>
                <p class="card-text">Quantity: {qty}</p>
                <button
                  className="btn bg-white m-1 btn-sm"
                  onClick={() => onIncrease(code)}
                >
                  Increase
                </button>
                <button
                  className={
                    qty > 0
                      ? "btn bg-white m-1 btn-sm"
                      : "btn bg-white m-1 disabled btn-sm"
                  }
                  onClick={() => onDecrease(code)}
                >
                  Decrease
                </button>
              </div>
            </div>
          </div>
        ) : (
          <div className="row border">
            <div className="col-3 ">{name}</div>
            <div className="col-2 ">{code}</div>
            <div className="col-2 ">{price}</div>
            <div className="col-2 ">{qty}</div>
            <div className="col-3 ">
              <button
                className="btn btn-success m-1 btn-sm"
                onClick={() => onIncrease(code)}
              >
                +
              </button>
              <button
                className={
                  qty > 0
                    ? "btn btn-danger m-1 btn-sm"
                    : "btn btn-danger m-1 disabled btn-sm"
                }
                onClick={() => onDecrease(code)}
              >
                -
              </button>
            </div>
          </div>
        )}
      </React.Fragment>
    );
  }
}
export default Store3;
