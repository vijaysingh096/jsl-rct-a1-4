import React, { Component } from "react";

import Book3 from "./task4.3.Book3";
import BookNavbar from "./task4.3.BookNavbar";
class LibrarySystem3 extends Component {
  state = {
    Books: [
      { name: "Harry Potter", author: "JK Rowling" },
      { name: "War and Peace", author: "Leo Tolstoy" },
      { name: "Malgudi Days", author: "RK Narayan" },
      { name: "Gitanjali", author: "RN Tagore" },
    ],
    IssueBooks: [],
  };
  handelIssueBooks = (Name) => {
    let s1 = { ...this.state };
    let book1 = s1.Books.find((b1) => b1.name === Name);
    let index = s1.Books.findIndex((b1) => b1.name === Name);
    s1.IssueBooks.push(book1);
    s1.Books.splice(index, 1);
    this.setState(s1);
  };
  handelRemove = (Name) => {
    let s1 = { ...this.state };
    let book1 = s1.IssueBooks.find((b1) => b1.name === Name);
    let index = s1.IssueBooks.findIndex((b1) => b1.name === Name);
    s1.Books.push(book1);
    s1.IssueBooks.splice(index, 1);
    this.setState(s1);
  };

  render() {
    let { Books, IssueBooks } = this.state;
    return (
      <React.Fragment>
        <BookNavbar
          noOfBooks={Books.length}
          noOfIssueBook={IssueBooks.length}
        />
        <div className="container">
          <h2 className="text-center">Books in Library</h2>
          {Books.length > 0 ? (
            <div className="row  text-center ">
              {Books.map((b1) => {
                return <Book3 book={b1} onIssueBook={this.handelIssueBooks} />;
              })}
            </div>
          ) : (
            <h5>Library is Empty</h5>
          )}
          <h2>Issued Books</h2>
          {IssueBooks.length <= 0 ? (
            <p>No Book has been Issued</p>
          ) : (
            <ul>
              {IssueBooks.map((b1) => {
                return (
                  <li>
                    {b1.name}{" "}
                    <button
                      className="btn btn-secondary m-2 btn-sm "
                      onClick={() => this.handelRemove(b1.name)}
                    >
                      Remove
                    </button>
                  </li>
                );
              })}
            </ul>
          )}
        </div>
      </React.Fragment>
    );
  }
}
export default LibrarySystem3;
