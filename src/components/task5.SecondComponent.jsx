import React, { Component } from "react";
class SecondComponent extends Component {
  render() {
    let { mobile, addCart } = this.props;
    let { name, RAM, features, price } = mobile;
    return (
      <React.Fragment>
        <div className="col-4 my-2">
          <div class="card bg-success text-center">
            <div class="card-body">
              <h5 class="card-title">
                {name}({RAM})
              </h5>
              <p class="card-text"> {features}</p>
              <button
                className="btn bg-white m-1 btn-sm"
                onClick={() => addCart(name)}
              >
                Just {price}{" "}
              </button>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
export default SecondComponent;
