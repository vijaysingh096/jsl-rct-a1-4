import React, { Component } from "react";

import Player2 from "./task2.2.Player2";
class PlayerSystem2 extends Component {
  state = {
    Players: ["Jack", "Steve", "Martha", "Bob", "Katherine"],
  };
  render() {
    let { Players } = this.state;
    return (
      <React.Fragment>
       <div className="container">
          <h3>List of Name</h3>
        {Players.map((p1) => {
          return <Player2 player={p1} />;
        })}
        </div>
      </React.Fragment>
    );
  }
}
export default PlayerSystem2;
